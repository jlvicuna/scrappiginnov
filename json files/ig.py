import os
from datetime import datetime
import pandas as pd
import instaloader
from glob import glob
import orjson
from openpyxl import Workbook

def getInstagramUrlFromMediaId(media_id):
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_'
    shortened_id = ''

    while media_id > 0:
        remainder = media_id % 64
        media_id = (media_id - remainder) // 64
        shortened_id = alphabet[int(remainder)] + shortened_id

    return 'https://instagram.com/p/' + shortened_id + '/'


def parse_instafiles(username, path):
    # os.chdir('C:\\Users\\juans\\Desktop\\UEES2\\pasantias\\json files')
    os.chdir('C:\\Users\\Lenovo\\Downloads\\App Redes\\json files')
    path = os.getcwd()

    os.chdir(os.path.join(path, username))
    
    print(os.getcwd())

    #Creando dataframe vacio solo con nombres de columnas 
    dataframe = pd.DataFrame()
  
    # glob busca los archivos del argumento en el directorio seleccionado anteriormente 
    # En este caso se buscan los que terminen en UTC.json 
    
    glob('*UTC.json')

    for file in reversed(glob('*UTC.json')):
        with open(file, 'r') as filecontent:
            filename = filecontent.name
            
            try:
                metadata = orjson.loads(filecontent.read())
            
            except IOError as e:
                print("I/O Error. Couldn't load file. Trying the next one...")
                continue
            else:
                pass
            
            time = datetime.fromtimestamp(int(metadata['node']['taken_at_timestamp']))
            type_ = metadata['node']['__typename']
            likes = metadata['node']['edge_media_preview_like']['count']  
            comments = metadata['node']['edge_media_to_comment']['count']
            username = '@' + metadata['node']['owner']['username']
            followers = metadata['node']['owner']['edge_followed_by']['count']
            
            try:
                text = metadata['node']['edge_media_to_caption']['edges'][0]['node']['text']
            except:
                text = ""
            try:
                post_id = metadata['node']['id']
            except:
                post_id = ""
            
            # Transformando post_id a URL de Instagram
            post_id = getInstagramUrlFromMediaId(int(post_id))

            minedata = {'Fecha': [time], 'Titulo': [text], 'Likes': [likes], 'Comentarios' : [comments], 
                        'Evidencia' : [post_id], 'Cuenta IG' : [username], 'Followers' : [followers]}
            
            #Se agrega el dataframe del archivo actual al dataframe general
            df_minedata = pd.DataFrame(minedata)
            dataframe = pd.concat([dataframe, df_minedata])
            
            #Cerrando el archivo actual
            del metadata
            filecontent.close()
    
    #Se agrega la columna 'source' donde se especifica la red social
    dataframe['Red Social'] = 'Instagram'
    return dataframe


# Username de la cuenta que se desea usar y nombre del archivo a guardar
username = 'eurekauees'
file = username + '.xlsx'

# Se llama a la funcion de parsing
df_instagram = parse_instafiles(username, os.getcwd() )
print(df_instagram)

# Creacion del archivo excel en base al dataframe creado
df_instagram.to_excel(os.path.join(os.getcwd(), file))


